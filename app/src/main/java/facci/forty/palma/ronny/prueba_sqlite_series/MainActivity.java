package facci.forty.palma.ronny.prueba_sqlite_series;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn_guardar,btn_consultar;

    EditText txt_serie,txt_genero,txt_protagonista,txt_anio,txt_temporadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_serie=findViewById(R.id.textSerie);
        txt_genero=findViewById(R.id.textGenero);
        txt_protagonista=findViewById(R.id.textProtagonista);
        txt_anio=findViewById(R.id.textAnio);
        txt_temporadas=findViewById(R.id.textTemporadas);
        btn_guardar=findViewById(R.id.btnGuardar);
        btn_consultar=findViewById(R.id.btnBuscar);



        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serie =txt_serie.getText().toString();
                String gener = txt_genero.getText().toString();
                String prota = txt_protagonista.getText().toString();
                String anio = txt_anio.getText().toString();
                String tempo = txt_temporadas.getText().toString();

                if(!serie.isEmpty() && !gener.isEmpty() && !prota.isEmpty() &&!anio.isEmpty()  &&!tempo.isEmpty() ){


                    guardar(serie,gener,prota,anio,tempo);

                }else {
                    Toast.makeText(MainActivity.this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_consultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Lista.class));
            }
        });
    }

    private void guardar(String Serie, String Genero, String Protagonista, String Anio, String Temporadas){
        DbSeries series = new DbSeries(this, "DbSeries",null,1);
        SQLiteDatabase db = series.getWritableDatabase();
        try {
            ContentValues c = new ContentValues();
            c.put("Serie",Serie);
            c.put("Genero",Genero);
            c.put("Protagonista",Protagonista);
            c.put("Anio",Anio);
            c.put("Temporadas",Temporadas);
            db.insert("SERIES","",c);
            db.close();
            Toast.makeText(this,"Serie registrada con exito",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(this,"Error:" + e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
}
