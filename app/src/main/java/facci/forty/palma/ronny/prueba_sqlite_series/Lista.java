package facci.forty.palma.ronny.prueba_sqlite_series;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Lista extends AppCompatActivity {

    ListView listViewSeries;
    ArrayList<String> listado;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        CargarListado();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        listViewSeries = findViewById(R.id.listViewSeries);

        CargarListado();

        listViewSeries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int clave = Integer.parseInt(listado.get(position).split(",  ")[0]);
                String serie = listado.get(position).split(",  ")[1];
                String genero = listado.get(position).split(",  ")[2];
                String protagonista = listado.get(position).split(",  ")[3];
                String anio = listado.get(position).split(",  ")[4];
                String temporadas = listado.get(position).split(",  ")[5];
                Toast.makeText(Lista.this,"SERIE: "+serie,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Lista.this,ActividadModificaryEliminar.class);
                intent.putExtra("Id",clave);
                intent.putExtra("Serie",serie);
                intent.putExtra("Genero",genero);
                intent.putExtra("Protagonista",protagonista);
                intent.putExtra("Anio",anio);
                intent.putExtra("Temporadas",temporadas);
                startActivity(intent);
            }
        });

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void CargarListado(){
        listado = ListaSeries();
        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
        listViewSeries.setAdapter(adapter);
    }

    private ArrayList<String> ListaSeries(){
        ArrayList<String> datos = new ArrayList<String>();
        DbSeries series = new DbSeries(this, "DbSeries",null,1);
        SQLiteDatabase db = series.getReadableDatabase();
        String sql ="select Id, Serie, Genero, Protagonista, Anio, Temporadas from Series";
        Cursor c = db.rawQuery(sql,null);
        if (c.moveToFirst()){
            do {
                String linea =c.getInt(0)+",  "+ c.getString(1)+",  "+c.getString(2)+",  "+c.getString(3)+",  "+c.getString(4)+",  "+c.getString(5);
                datos.add(linea);
            }while (c.moveToNext());
        }
        db.close();
        return datos;
    }
}