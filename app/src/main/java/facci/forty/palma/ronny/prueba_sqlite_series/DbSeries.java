package facci.forty.palma.ronny.prueba_sqlite_series;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbSeries extends SQLiteOpenHelper {

    String tabla="CREATE TABLE SERIES (ID INTEGER PRIMARY KEY, SERIE TEXT, GENERO  TEXT, PROTAGONISTA TEXT, ANIO TEXT, TEMPORADAS TEXT)";
    public DbSeries(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tabla);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table series");
        db.execSQL(tabla);
    }
}
