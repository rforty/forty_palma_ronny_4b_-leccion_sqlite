package facci.forty.palma.ronny.prueba_sqlite_series;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadModificaryEliminar extends AppCompatActivity {

    Button btn_modificar,btn_eliminar;
    EditText txt_serie,txt_genero,txt_protagonista,txt_anio,txt_temporadas;

    int id;
    String serie;
    String genero;
    String protagonista;
    String anio;
    String temporadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_modificary_eliminar);

        Bundle b = getIntent().getExtras();
        if (b!=null){
            id =b.getInt("Id");
            serie=b.getString("Serie");
            genero=b.getString("Genero");
            protagonista=b.getString("Protagonista");
            anio=b.getString("Anio");
            temporadas=b.getString("Temporadas");
        }

        txt_serie=findViewById(R.id.textSerie);
        txt_genero=findViewById(R.id.textGenero);
        txt_protagonista=findViewById(R.id.textProtagonista);
        txt_anio=findViewById(R.id.textAnio);
        txt_temporadas=findViewById(R.id.textTemporadas);
        btn_modificar=findViewById(R.id.btnModificar);
        btn_eliminar=findViewById(R.id.btnEliminar);

        txt_serie.setText(serie);
        txt_genero.setText(genero);
        txt_protagonista.setText(protagonista);
        txt_anio.setText(anio);
        txt_temporadas.setText(temporadas);

        btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Eliminar(id);
                onBackPressed();
                Toast.makeText(ActividadModificaryEliminar.this, "Serie Eliminada", Toast.LENGTH_SHORT).show();
            }
        });

        btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(id,txt_serie.getText().toString(),txt_genero.getText().toString(),txt_protagonista.getText().toString(),txt_anio.getText().toString(),txt_temporadas.getText().toString());
                Toast.makeText(ActividadModificaryEliminar.this, "Serie Actualizada", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });
    }

    private void Modificar(int Id,String Serie, String Genero, String Protagonista, String Anio, String Temporadas){

        DbSeries series = new DbSeries(this, "DbSeries",null,1);
        SQLiteDatabase db = series.getWritableDatabase();

        String sql ="update Series set Serie='"+ Serie
                +"',Genero='"+ Genero + "',Protagonista='"+ Protagonista + "',Anio='"+ Anio + "',Temporadas='"+ Temporadas +"' where Id="+Id;
        db.execSQL(sql);
        db.close();
    }

    private void Eliminar(int Id){
        DbSeries series = new DbSeries(this, "DbSeries",null,1);
        SQLiteDatabase db = series.getWritableDatabase();

        String sql = "delete from Series where Id="+Id;
        db.execSQL(sql);
        db.close();
    }
}

